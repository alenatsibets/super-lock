import java.util.Random;

public class SuperLock {
    public static void main(String[] args) {
        int[] pin = new int[10];
        initialize(pin);
        for (int i = 2; i < pin.length; i++){
            pin[i] = determination(pin[i-2], pin[i-1]);
        }
        display(pin);
    }
    public static void initialize(int[] arr) {
        Random rand = new Random();
        arr[0] = rand.nextInt(6);
        while (arr[1] + arr[0] < 4) {
            arr[1] = rand.nextInt(6);
        }
    }
    public static int determination (int first, int second){
        int sum = first + second;
        return 10 - sum;
    }
    public static void display(int[] array){
        for (int element : array) {
            System.out.print(element + " ");
        }
    }
}